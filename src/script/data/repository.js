import jsonBlob from "./jsonBlob";

/**
 * Provides methods for persisting JSON data
 */
export default class {
	/**
	 * get the current repo id
	 *
	 * @returns {String}
	 */
	get id() {
		return this._id;
	}

	/**
	 * Initialize repo with given id.
	 * If no id is given a new unique repo id is created.
	 * If an id was already given in constructor it is returned.
	 *
	 * @param {String} [id] repo id
	 * @returns {Promise} resolves with given or created id
	 */
	init(id) {
		if (id) {
			this._id = id;
			return Promise.resolve(this._id);
		}
		return jsonBlob.post({})
			.then((createdId) => {
				this._id = createdId;
				return createdId;
			});
	}

	/**
	 * save repo data
	 *
	 * @returns {Promise}
	 */
	save(state) {
		return jsonBlob.put(this._id, state);
	}


	/**
	 * load repo data
	 *
	 * @returns {Promise}
	 */
	load() {
		return jsonBlob.get(this._id);
	}
}
