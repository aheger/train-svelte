const warmupReps = [
	{ count: 5, ratio: 0.40 },
	{ count: 5, ratio: 0.50 },
	{ count: 3, ratio: 0.60 }
];

let repScheme = [
	[
		{ count: 5, ratio: 0.65 },
		{ count: 5, ratio: 0.75 },
		{ count: 5, ratio: 0.85 }
	],
	[
		{ count: 3, ratio: 0.70 },
		{ count: 3, ratio: 0.80 },
		{ count: 3, ratio: 0.90 }
	],
	[
		{ count: 5, ratio: 0.75 },
		{ count: 3, ratio: 0.85 },
		{ count: 1, ratio: 0.95 }
	],
	[
		{ count: 5, ratio: 0.6 },
		{ count: 5, ratio: 0.6 },
		{ count: 5, ratio: 0.6 }
	]
];

let assistScheme = {
	"ohp": ["pullup", "dip"],
	"deadlift": ["stiff-leg-deadlift", "leg-raise"],
	"bench": ["pullup", "pushup"],
	"front-squat": ["bw-split-squat", "situp"]
};

repScheme = repScheme.map((cur) => {
	return warmupReps.concat(cur);
});

function getAssistance(ix) {
	return assistScheme[ix].map((cur) => {
		return {
			liftId: cur,
			reps: [
				{ count: 10 },
				{ count: 10 },
				{ count: 10 },
				{ count: 10 },
				{ count: 10 },
				{ count: 10 },
				{ count: 10 },
				{ count: 10 }
			]
		};
	});
}

function getDay(id, name, ix) {
	return {
		name,
		sets: [
			{
				liftId: id,
				reps: repScheme[ix]
			}
		].concat(getAssistance(id))
	};
}

function getWeek(ix) {
	return [
		getDay("ohp", `Press ${ix + 1}`, ix),
		getDay("deadlift", `Deadlift ${ix + 1}`, ix),
		getDay("bench", `Bench ${ix + 1}`, ix),
		getDay("front-squat", `Squat ${ix + 1}`, ix)
	];
}

const days = [0, 1, 2, 3].reduce((prev, curr) => prev.concat(getWeek(curr)), []);

export default {
	days: days
};
