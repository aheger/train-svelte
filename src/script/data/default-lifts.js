const lifts = {
	"ab-roller": {
		name: "Ab Roller",
		weight: 0
	},
	"bb-row": {
		name: "Barbell Row",
		weight: 102.5
	},
	"bw-split-squat": {
		name: "BW Split Squat",
		weight: 0
	},
	"bench": {
		name: "Bench Press",
		weight: 100
	},
	"calf-raise": {
		name: "Calf Raise",
		weight: 115
	},
	"cg-bench": {
		name: "Closegrip Bench",
		weight: 97.5
	},
	"curl": {
		name: "Curl",
		weight: 40
	},
	"deadlift": {
		name: "Deadlift",
		weight: 160
	},
	"dip": {
		name: "Dip",
		weight: 0
	},
	"db-reverse-fly": {
		name: "Dumbbell Reverse Fly",
		weight: 45
	},
	"db-ohp": {
		name: "Dumbbell OHP",
		weight: 62.5
	},
	"db-row": {
		name: "Dumbbell Row",
		weight: 62.5
	},
	"ghr": {
		name: "Glute-Ham Raise",
		weight: 0
	},
	"front-squat": {
		name: "Front Squat",
		weight: 102.5
	},
	"incline-bench": {
		name: "Incline Bench",
		weight: 97.5
	},
	"jefferson-deadlift": {
		name: "Jefferson Deadlift",
		weight: 145
	},
	"lat-raise": {
		name: "Lateral Raise",
		weight: 35
	},
	"leg-raise": {
		name: "Leg Raise",
		weight: 0
	},
	"ohp": {
		name: "Overhead Press",
		weight: 60
	},
	"oh-squat": {
		name: "Overhead Squat",
		weight: 45
	},
	"one-leg-deadlift": {
		name: "One Leg Deadlift",
		weight: 57.5
	},
	"pullup": {
		name: "Pullup",
		weight: 0
	},
	"pushup": {
		name: "Pushup",
		weight: 0
	},
	"recl-curl": {
		name: "Reclined Curl",
		weight: 40
	},
	"reverse-lunge": {
		name: "Reverse Lunge",
		weight: 87.5
	},
	"situp": {
		name: "Situp",
		weight: 0
	},
	"snatch-deadlift": {
		name: "Snatch Deadlift",
		weight: 80
	},
	"split-squat": {
		name: "Split Squat",
		weight: 80
	},
	"squat": {
		name: "Squat",
		weight: 125
	},
	"stiff-leg-deadlift": {
		name: "Stiff-Leg Deadlift",
		weight: 60
	},
	"thread-needle": {
		name: "Thread the Needle",
		weight: 17.5
	},
	"triceps-ext": {
		name: "Triceps Extension",
		weight: 42.5
	},
	"uh-db-bench": {
		name: "Underhand DB BP",
		weight: 70
	}
};

export default lifts;
