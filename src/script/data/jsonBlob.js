// api wrapper for https://jsonblob.com
import "whatwg-fetch";

const baseUrl = "https://jsonblob.com/api/jsonBlob";
const headers = {
	"Content-Type": "application/json"
};

function checkStatus(response) {
	if (response.status >= 200 && response.status < 300) {
		return response;
	}

	const error = new Error(response.statusText);
	error.response = response;
	throw error;
}

function parseJSON(response) {
	return response.json();
}

function extractLocation(response) {
	const location = response.headers.get("location");
	return location.match(/\/([^/]*)$/)[1];
}

export default {
	get(id) {
		return fetch(`${baseUrl}/${id}`, {
				method: "GET",
				headers
			})
			.then(checkStatus)
			.then(parseJSON);
	},
	post(data) {
		return fetch(baseUrl, {
				method: "POST",
				headers,
				body: JSON.stringify(data)
			})
			.then(checkStatus)
			.then(extractLocation);
	},
	put(id, data) {
		return fetch(`${baseUrl}/${id}`, {
			method: "PUT",
			headers,
			body: JSON.stringify(data)
		})
		.then(checkStatus);
	}
};
