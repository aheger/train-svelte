import Alert from "../component/dialog/alert.svelte";
import Confirm from "../component/dialog/confirm.svelte";
import Lift from "../component/dialog/lift.svelte";

function _createDialog(klass, data) {
	return new Promise((res, rej) => {
		const instance = new klass({
			target: document.body,
			data: data
		});

		instance.on("confirm", res);
		instance.on("cancel", rej);
	});
}

export default {
	alert: _createDialog.bind(null, Alert),
	confirm: _createDialog.bind(null, Confirm),
	lift: _createDialog.bind(null, Lift)
};
