import createRouter from "router5";
import browserPlugin from "router5/plugins/browser";
import listenersPlugin from "router5/plugins/listeners";

const routes = [
	{ name: "dash", path: "/dash" },
	{ name: "lifts", path: "/lifts" },
	{ name: "program", path: "/program" },
	{ name: "settings", path: "/settings" },
	{ name: "workout", path: "/workout" }
];

const router = createRouter(routes, {
	defaultRoute: "dash"
});

router.usePlugin(browserPlugin({
	useHash: true
}));

router.usePlugin(listenersPlugin());

export default router;
