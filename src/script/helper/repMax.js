const FORMULAS = {
	NONE: 0,
	BRZYCKI: 1,
	EPLEY: 2,
	LANDER: 4,
	ALL: 8 - 1
};

class RepMaxCalc {
	constructor(formulas) {
		// select all formulas as default
		this._selectedFormulas = formulas || FORMULAS.ALL;

		// define calculation functions
		this._formulas = {};
		// define inverted calculation functions
		this._formulasInv = {};

		// BRZYCKI
		this._formulas[FORMULAS.BRZYCKI] = (weight, reps) => {
			return weight * (36.0 / (37.0 - reps));
		};
		this._formulasInv[FORMULAS.BRZYCKI] = (repMax, reps) => {
			return (repMax * (37.0 - reps)) / 36.0;
		};

		// EPLEY
		this._formulas[FORMULAS.EPLEY] = (weight, reps) => {
			return weight * (1.0 + reps / 30.0);
		};
		this._formulasInv[FORMULAS.EPLEY] = (repMax, reps) => {
			return repMax / (1.0 + reps / 30.0);
		};

		// LANDER
		this._formulas[FORMULAS.LANDER] = (weight, reps) => {
			return (100 * weight) / (101.3 - 2.67123 * reps);
		};
		this._formulasInv[FORMULAS.LANDER] = (repMax, reps) => {
			return (repMax * (101.3 - 2.67123 * reps)) / 100;
		};
	}

	/**
	 * Apply given formulas if they are selected
	 *
	 * @param {Object} formulas formula object
	 * @param {Number} par1 first formula parameter
	 * @param {Number} par2 second formula parameter
	 * @returns {Number} formula result
	 */
	_applySelectedFormulas(formulas, par1, par2) {
		if (!par1 || par1 < 0 || !par2 || par2 < 0) {
			return Number.NaN;
		}

		// array of results
		let res = [];


		// for each formula flag
		for (let curFormulaFlag of Object.values(FORMULAS)) {
			// ...that is selected and has a function defined
			if (this._isFormulaSelected(curFormulaFlag) && formulas[curFormulaFlag]) {
				// push the formula result to result array if valid
				var curRes = formulas[curFormulaFlag](par1, par2);

				if (isFinite(curRes) && !isNaN(curRes)) {
					res.push(curRes);
				}
			}
		}

		let ret = Number.NaN;

		if (res.length > 0) {
			// calc the average of results
			ret = res.reduce(function(prev, curr) {
				prev = prev + curr;
				return prev;
			}, 0) / res.length;
		}

		return ret;
	}

	/**
	 * returns if given formula flag is selected
	 *
	 * @param {int} flg formula flag from RepMaxCalc.prototype.FORMULAS
	 * @returns true if formula is selected
	 */
	_isFormulaSelected(flg) {
		return (this._selectedFormulas & flg) === flg;
	}

	/**
	 * calculates the repetition max for given weight and reps
	 * based on the selected calculation formulas
	 *
	 * @param {Number} weight lifted weight in any weight unit
	 * @param {Number} reps number of reps done
	 */
	getRepMax(weight, reps) {
		return this._applySelectedFormulas(this._formulas, weight, reps);
	}

	/**
	 * get weight by rep max and performed repetitions
	 *
	 * @param {Number} repMax repetition max for which to find weight
	 * @param {Number} reps number of reps done
	 */
	getWeight(repMax, reps) {
		return this._applySelectedFormulas(this._formulasInv, repMax, reps);
	}
}

RepMaxCalc.FORMULAS = FORMULAS;

export default new RepMaxCalc();

