export function hashString(str) {
	let hash = 0,
		i;

	if (str.length === 0) {
		return hash;
	}

	for (i = 0; i < str.length; i++) {
		hash = ((hash << 5) - hash) + str.charCodeAt(i);
		hash |= 0; // Convert to 32bit integer
	}
	return hash;
}

export function hashObject(obj) {
	return hashString(JSON.stringify(obj));
}
