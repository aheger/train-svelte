/**
 * Debounce given function
 *
 * @param {Function} func the function to debounce
 * @param {Number} wait wait interval
 * @returns {Function} debounced function
 */
export default function(func, wait) {
	let timeout;

	return function() {
		// create funcion call with persisted this
		const funcCall = () => func.apply(this, arguments);

		// clear existing timeout
		clearTimeout(timeout);

		// set the timeout
		timeout = setTimeout(funcCall, wait);
	};
}
