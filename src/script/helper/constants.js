const constants = {
	repStates: {
		REP_STATE_NONE: 0,
		REP_STATE_DONE: 1,
		REP_STATE_FAILED: 2
	},
	keyCodes: {
		d: 100,
		f: 102,
		space: 32
	},
	// types of how to progress a lift after a finished workout period
	progTypes: {
		// increase by fixed weight
		PROG_TYPE_FIXED: 0,
		// increase by ratio
		PROG_TYPE_RATIO: 1
	}
};

export default constants;
