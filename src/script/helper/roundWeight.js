export default (weight) => {
	var lower = (weight / 2.5) >> 0;
	var upper = lower + 1;

	lower *= 2.5;
	upper *= 2.5;

	if (weight - lower > upper - weight) {
		return upper;
	}

	return lower;
};
