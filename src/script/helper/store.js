import constants from "./constants";
import { hashObject } from "./hash";
import { clone } from "./object";
import roundWeight from "./roundWeight";

const { progTypes, repStates } = constants;

export function getResultState(states) {
	let allDone = true,
		oneFailed = false;

	states.forEach(state => {
		switch (state) {
			case repStates.REP_STATE_DONE:
				break;
			case repStates.REP_STATE_FAILED:
				oneFailed = true;
				break;
			default:
				allDone = false;
				break;
		}
	});

	if (allDone) {
		return oneFailed ? repStates.REP_STATE_FAILED : repStates.REP_STATE_DONE;
	}

	return repStates.REP_STATE_NONE;
}

export function createWorkoutState(program) {
	return {
		programHash: hashObject(program),
		state: repStates.REP_STATE_NONE,
		days: program.days.map(day => {
			return {
				state: repStates.REP_STATE_NONE,
				sets: day.sets.map(set => {
					return {
						state: repStates.REP_STATE_NONE,
						reps: set.reps.map(() => {
							return repStates.REP_STATE_NONE;
						})
					};
				})
			};
		})
	};
}

/**
 * Compile the states of each lift in the given workout to a resulting state.
 *
 * @param {Object} program program object
 * @param {Object} workout workout object
 * @returns {Object} result state object
 */
function reduceLiftStates(program, workout) {
	const states = {};

	workout.days.forEach((day, dayIndex) => {
		day.sets.forEach((set, setIndex) => {
			const liftId = program.days[dayIndex].sets[setIndex].liftId;

			if (!states[liftId]) {
				states[liftId] = [];
			}
			states[liftId].push(set.state);
		});
	});

	Object.keys(states).forEach(liftId => {
		states[liftId] = getResultState(states[liftId]);
	});

	return states;
}

/**
 * Apply lift progression to lift based on state
 *
 * @param {Object} lift lift object
 * @param {Object} liftProg lift progression object
 * @param {Number} state rep state to apply
 * @returns {Object} new lift object
 */
function applyProgression(lift, liftProg, state) {
		// If lift is done, increase weight by progression value else reduce by 3 x progression value
	let delta = state === repStates.REP_STATE_DONE ? 1 : -3;

	switch (liftProg.type) {
		case progTypes.PROG_TYPE_FIXED:
			delta *= liftProg.value;
			break;
		case progTypes.PROG_TYPE_RATIO:
			delta *= roundWeight(lift.weight * liftProg.value);
			break;
		default:
			delta = 0;
			break;
	}

	return Object.assign({}, lift, { weight: lift.weight + delta });
}

/**
 * Progress lifts according to workout result using the given progression
 * strategy.
 *
 * @param {Object} lifts lifts object
 * @param {Object} workout workout object
 * @param {Object} program program object
 * @param {Array} progression progression strategies
 * @returns {Object} new lifts object
 */
export function progressLifts(lifts, workout, program, progression) {
	const ret = clone(lifts),
		liftStates = reduceLiftStates(program, workout);

	Object.entries(liftStates).forEach(([liftId, liftState]) => {
		if (!liftState) {
			return;
		}

		const liftProg = progression.find((curProgress) => liftId.match(curProgress.matcher));

		if (liftProg) {
			ret[liftId] = applyProgression(ret[liftId], liftProg, liftState);
		}
	});

	return ret;
}

