const _data = {
	"app.title": "train-svelte",
	"app.loading": "Loading…",
	"dialog.cancel": "Cancel",
	"dialog.close": "Close",
	"dialog.lift.label.label": "Label",
	"dialog.lift.label.weight": "Weight",
	"dialog.ok": "Ok",
	"page.dash": "Dash",
	"page.lifts": "Lifts",
	"page.lifts.description": "Edit your lifts",
	"page.program": "Program",
	"page.program.description": "Edit your program",
	"page.settings": "Settings",
	"page.settings.description": "Change general settings",
	"page.workout": "Workout",
	"page.workout.description": "Start your workout",
	"page.workout.next": "Next",
	"page.workout.previous": "Prev",
	"page.workout.finishCycle": "Finish cycle",
	"page.workout.finishCycle.explanation": "Finish your current workout cycle and start a new one",
	"restTimer.hint": "End rest"
};

const _lang = (key) => {
	return _data[key] || `MISS ${key}`;
};

export default _lang;
