import { Store } from "svelte/store";
import { getResultState, createWorkoutState, progressLifts } from "./helper/store";
import DefaultProgram from "./data/default-program";
import DefaultProgress from "./data/default-progress";
import DefaultLifts from "./data/default-lifts";
import Repository from "./data/repository";
import constants from "./helper/constants";
import debounce from "./helper/debounce";

const { repStates } = constants,
	restInterval = 90,
	// list of properties that should be persisted between sessions
	persistableProps = [
		"workout",
		"lifts"
	];

class TrainStore extends Store {
	/**
	 * Initialize the store for given store id.
	 * If no store id is given, a new random store id is created.
	 *
	 * @returns {Promise} resolves with store id
	 */
	init(initStoreId) {
		this._repo = new Repository();

		function watchRepo(storeId) {
			// create a debounced save function
			const debouncedSave = debounce(() => {
					this.savePersistableState().then(() => {
						// reset saving flag when save is finisehd
						this.set({ isSaving: false });
					});
				}, 1000);

			const saveFunc = () => {
					// set 'saving' flag
					this.set({ isSaving: true });
					debouncedSave();
				};

			// persist the new persistable state on change
			this.on("state", ({ changed }) => {
				// check each persistable property
				const doSave = persistableProps.some(prop => changed[prop]);

				if (doSave) {
					saveFunc();
				}
			});

			// result pass-through
			return storeId;
		}

		function applyRepo(storeId) {
			this.set({ storeId: storeId });

			if (initStoreId) {
				// there was an initial store id
				// -> load pre-existing state
				return this.loadPersistableState()
					.then(() => storeId);
			}

			// result pass-through
			return storeId;
		}

		return this._repo.init(initStoreId)
			.then(applyRepo.bind(this))
			.then(watchRepo.bind(this));
	}

	/**
	 * save the persistable state
	 *
	 * @returns {Promise}
	 */
	savePersistableState() {
		// get subset of state that should be persisted between sessions
		const persistableState =
			persistableProps.reduce((res, curProp) => {
				res[curProp] = this.get()[curProp];
				return res;
			}, {});

		// save it to repo
		return this._repo.save(persistableState);
	}

	/**
	 * load the persistable state
	 *
	 * @returns {Promise}
	 */
	loadPersistableState() {
		return this._repo.load()
			.then((loaded) => {
				const { workout, lifts } = this.get();

				let newState = { workout, lifts };

				// check if the loaded workout still fits the program
				if (loaded.workout && workout.programHash === loaded.workout.programHash) {
					// the loaded workout belongs to the same program as the
					// current workout -> apply it
					newState.workout = loaded.workout;
				}

				// extend the default lifts with the loaded lifts
				Object.assign(newState.lifts, loaded.lifts);

				this.set(newState);
				this.continueWorkout();
				// result pass-through
				return loaded;
			});
	}
	clearTimer() {
		if (this._restTimer) {
			clearInterval(this._restTimer);
		}
		this.set({ restTimer: null });
	}
	startTimer() {
		this.clearTimer();

		this._restEnd = new Date();
		this._restEnd.setSeconds(this._restEnd.getSeconds() + restInterval);


		this.set({ restTimer: restInterval });

		this._restTimer = setInterval(() => {
			let restLeft = Math.round((this._restEnd - new Date()) / 1000);

			this.set({ restTimer: restLeft });

			if (restLeft <= 0) {
				this.clearTimer();
			}

		}, 1000);
	}
	switchDay(delta) {
		const dayCount = this.get().program.days.length;

		let dayIndex = this.get().dayIndex;

		dayIndex += delta;

		if (dayIndex < 0) {
			dayIndex = dayCount - 1;
		} else if (dayIndex >= dayCount) {
			dayIndex = 0;
		}

		this.set({
			dayIndex
		});
	}
	/**
	 * continue workout in a new session
	 */
	continueWorkout() {
		const cursor = this.getCurrentRepCursor();

		if (cursor) {
			// workout is unfinished, set day index to first unfinished day
			this.set({
				dayIndex: cursor[0]
			});
		}
	}
	/**
	 * Start a new workout cycle
	 */
	startCycle() {
		const { lifts, workout, program } = this.get(),
			progressed = progressLifts(lifts, workout, program, DefaultProgress);

		this.set({
			dayIndex: 0,
			lifts: progressed,
			workout: createWorkoutState(program)
		});
	}
	switchRepState(dayIndex, setIndex, repIndex, forceState) {
		const workout = this.get().workout,
			day = workout.days[dayIndex],
			set = day.sets[setIndex],
			curState = set.reps[repIndex] || repStates.REP_STATE_NONE;

		let newState;

		switch (curState) {
			case repStates.REP_STATE_NONE:
				this.startTimer();
				newState = repStates.REP_STATE_DONE;
				break;
			case repStates.REP_STATE_DONE:
				newState = repStates.REP_STATE_FAILED;
				break;
			default:
				newState = repStates.REP_STATE_NONE;
				break;
		}

		if (typeof forceState !== "undefined") {
			newState = forceState;
		}

		set.reps[repIndex] = newState;

		set.state = getResultState(set.reps);
		day.state = getResultState(day.sets.map(set => set.state));
		workout.state = getResultState(workout.days.map(day => day.state));

		this.set({ workout });
	}
	/**
	 * Returns index array that points to the current unfinished repetition in
	 * the workout.
	 *
	 * @returns {Array} array of [dayIndex, setIndex, repIndex] or null if there is none
	 */
	getCurrentRepCursor() {
		const workout = this.get().workout;

		// find unfinished day
		const dayIndex = workout.days.findIndex(day => !day.state);
		if (dayIndex < 0) {
			return null;
		}

		const day = workout.days[dayIndex],
			setIndex = day.sets.findIndex(set => !set.state),
			set = day.sets[setIndex],
			repIndex = set.reps.findIndex(rep => !rep);

		return [dayIndex, setIndex, repIndex];
	}
	/**
	 * Update given lift id with given data
	 */
	updateLift(liftId, data) {
		const lifts = this.get().lifts,
			resultLift = Object.assign({}, lifts[liftId], data);

		lifts[liftId] = resultLift;

		this.set({ lifts });
	}
}

const workoutState = createWorkoutState(DefaultProgram);

const store = new TrainStore({
	dayIndex: 0,
	lifts: DefaultLifts,
	program: DefaultProgram,
	restTimer: null,
	workout: workoutState
});

export default store;
