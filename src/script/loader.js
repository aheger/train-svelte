import Loader from "../component/loader.svelte";

const _loaderInst = new Loader({
	target: document.querySelector("#loader")
});

let _stack = 0;

export default {
	push(msg = "") {
		_loaderInst.set({
			msg
		});
		_stack++;
		document.body.classList.add("is-loading");
	},
	pop() {
		_stack--;
		if (_stack <= 0) {
			document.body.classList.remove("is-loading");
			_stack = 0;
		}
	}
};
