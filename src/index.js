import App from "./component/app.svelte";
import loader from "./script/loader";
import lang from "./script/lang";
import store from "./script/store";
import router from "./script/router";

import "./style/_vars.css";
import "./style/index.css";
import "./style/app.css";
import "./style/animation.css";
import "./style/container.css";
import "./style/dash.css";
import "./style/form.css";
import "./style/header.css";
import "./style/icon.css";
import "./style/lift.css";
import "./style/loader.css";
import "./style/modal.css";
import "./style/page.css";
import "./style/sidebar.css";
import "./style/ui.css";
import "./style/workout.css";


document.title = lang("app.title");

loader.push(lang("app.loading"));

// try to determine existing store id from location.hash
const match = (location.hash || "").match(/user\/([^/]*)(\/|$)/),
	storeId = match ? match[1] : undefined;

// initialize the store
store.init(storeId).then((storeId) => {
	// prepend routes with the store id
	router.setRootPath(`/user/${storeId}`);

	new App({
		target: document.querySelector("#app")
	});
	router.start();
	loader.pop();
});

