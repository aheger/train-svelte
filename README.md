# train-svelte

Training calculator for weightlifting.

[Demo](https://aheger.gitlab.io/train-svelte/)

## Project Goals

* provide a way to calculate a weightlifting training program
* use Svelte framework
