const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const mode = process.env.NODE_ENV || "production";
const prod = mode === "production";

module.exports = {
	mode,
	output: {
		path: __dirname + "/public",
		filename: "[name].[hash].js"
	},
	module: {
		rules: [
			{
				test: /\.svelte$/,
				use: {
					loader: "svelte-loader",
					options: {
						skipIntroByDefault: true,
						nestedTransitions: true,
						hotReload: !prod,
						emitCss: true
					}
				}
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
					options: {
						presets: ["babel-preset-env"]
					}
				}
			},
			{
				test: /\.html$/,
				use: [
					{
						loader: "html-loader",
						options: { minimize: true }
					}
				]
			},
			{
				test: /\.css$/,
				/**
				 * MiniCssExtractPlugin doesn't support HMR.
				 * For developing, use 'style-loader' instead.
				 * */
				use: [
					prod ? MiniCssExtractPlugin.loader : "style-loader",
					{
						loader: "css-loader",
						options: {
							minimize: prod
						}
					},
					{
						loader: "postcss-loader",
							options: {
								config: {
									path: __dirname + "/postcss.config.js"
								}
							}
					}
				]
			},
			{
				test: /\.(ttf|woff|woff2)$/,
				use: [
					{
						loader: "file-loader",
						options: {
							name: "[name].[hash].[ext]"
						}
					}

				]
			}
		]
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: "./src/static/index.html",
			filename: "./index.html",
			favicon: "./src/static/favicon.ico"
		}),
		new MiniCssExtractPlugin({
			filename: "[name].[hash].css"
		})
	]
};
